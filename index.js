const binarySearchInsert = require('binary-search-insert')

module.exports = class SortedMap {
  /**
   * Creates a sorted map with all the method and properties of a regular [JS Map](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Map)
   * @param {Array.<Array>} [values] - the intial values for the Map
   * @param {Function} compare - A comparator function that takes two arguments and returns a number. The first argument will be a member of sortedArray, the second argument will be item.
   *   If item < member, return value < 0
   *   If item > member, return value > 0
   */
  constructor (values, compare) {
    if (typeof values === 'function') {
      compare = values
      values = []
    }

    this._compare = function _compare (a, b) {
      return compare(a[1], b[1])
    }

    const array = []
    for (const val of values) {
      binarySearchInsert(array, this._compare, val)
    }
    this._map = new Map(array)

    const methods = ['clear', 'delete', 'entries', 'forEach', 'get', 'has', 'keys', 'values', Symbol.iterator]
    const self = this
    methods.forEach(method => {
      self[method] = function () {
        return self._map[method].apply(self._map, arguments)
      }
    })
  }

  get size () {
    return this._map.size
  }

  get [Symbol.toStringTag] () {
    return 'SortedMap'
  }

  set (key, value) {
    const array = [...this._map]
    binarySearchInsert(array, this._compare, [key, value])
    this._map = new Map(array)
  }
}
