const tape = require('tape')
const SortedMap = require('../index.js')

tape('tests', (t) => {
  const map = new SortedMap([
    ['a', 7],
    ['b', 1],
    ['c', 3]
  ], function (a, b) {
    return a - b
  })

  t.equals(map.get('a'), 7)

  map.set('z', 6)
  t.deepEquals([...map], [
    ['b', 1],
    ['c', 3],
    ['z', 6],
    ['a', 7]
  ])

  const map2 = new SortedMap((a, b) => {
    return a - b
  })

  map2.set('a', 9)
  map2.set('b', 1)

  t.deepEquals([...map2], [
    ['b', 1],
    ['a', 9]
  ])
  t.equals(map2.size, 2)
  t.equals(map2.toString(), '[object SortedMap]')
  t.end()
})
