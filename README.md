# SYNOPSIS 
[![NPM Package](https://img.shields.io/npm/v/sortedmap.svg?style=flat-square)](https://www.npmjs.org/package/sortedmap)
[![Build Status](https://img.shields.io/travis/wanderer/sortedmap.svg?branch=master&style=flat-square)](https://travis-ci.org/wanderer/sortedmap)
[![Coverage Status](https://img.shields.io/coveralls/wanderer/sortedmap.svg?style=flat-square)](https://coveralls.io/r/wanderer/sortedmap)

[![js-standard-style](https://cdn.rawgit.com/feross/standard/master/badge.svg)](https://github.com/feross/standard)  

# INSTALL
`npm install sortedmap`

# USAGE

```javascript
const SortedMap = require('sortedmap')

const map = new SortedMap([
  ['a', 7],
  ['b', 1],
  ['c', 3]
], (a, b) => {
  return a - b
})

map.get('a') // 7

map.set('z', 6)
[...map] // [['b', 1], ['c', 3], ['z', 6], ['a', 7]]
```

# API

## constructor

[index.js:11-27](https://github.com/wanderer/sortedmap/blob/303a3a16bb252c4f6328065f4f599ba68da8588f/index.js#L11-L27 "Source code on GitHub")

Creates a sorted map with all the method and properties of a regular [JS Map](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Map)

**Parameters**

-   `values` **[Array](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array)&lt;[Array](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array)>?** the intial values for the Map
-   `compare` **[Function](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/function)** A comparator function that takes two arguments and returns a number. The first argument will be a member of sortedArray, the second argument will be item.
      If item &lt; member, return value &lt; 0
      If item > member, return value > 0

# LICENSE
[MPL-2.0](https://tldrlegal.com/license/mozilla-public-license-2.0-(mpl-2))
